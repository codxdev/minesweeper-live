#pragma once

#include <string>
#include <Windows.h>

namespace Utils
{
	// Generates a random number between <paramref name="min"/> and <paramref name="max"/> (inclusive range)
	// <param name="min">The minimum generated number (included)</param>
	// <param name="max">The maximum generated number (included)</param>
	// <returns>A random number in the range</returns>
	const int GetRandom(const int min, const int max);
	// Converts a byte string to a wide char string
	// <param name="s>Byte string to convert</param>
	// <returns>The wide char string corresponding to the passed byte string</returns>
	std::wstring s2ws(const std::string & s);
	// Converts a wide char string to a byte string
	// <param name="s>Wide char string to convert</param>
	// <returns>The byte string corresponding to the passed wide char string</returns>
	std::string ws2s(const std::wstring & s);
}

#define VECTOR_CONTAINS(vector, element) std::find_if(vector.begin(), vector.end(), element) != invalidCoords.end()
#define SET_CONSOLE_POINT(handle, x, y) { \
		COORD _crd; \
		_crd.X = x; \
		_crd.Y = y; \
		SetConsoleCursorPosition(handle, _crd); \
	}
#define CONSOLE_CLS system("cls")
#define CONSOLE_WAIT system("pause")
#define STR_TOUPPER(str) std::transform(str.begin(), str.end(), str.begin(), ::toupper)
