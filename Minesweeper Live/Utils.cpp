#include "Utils.h"
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <stringapiset.h>

using namespace std;

namespace Utils
{
	unsigned Hash3(unsigned h1, unsigned h2, unsigned h3) { return ((h1 * 3467895678U) - h2) + h3; }
	unsigned long int randomsCount = 0;
}
const int Utils::GetRandom(const int min, const int max)
{
	unsigned seed;
	seed = Hash3((unsigned)time(NULL), (randomsCount += randomsCount / 2 + 1), (unsigned int)&seed);
	srand(seed);
	return min + (int)round(rand() % (max + 1 - min));
}
std::wstring Utils::s2ws(const std::string & s)
{
	int len;
	int slength = (int)s.length();
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	std::wstring r(len, L'\0');
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, &r[0], len);
	return r;
}
std::string Utils::ws2s(const std::wstring & s)
{
	int len;
	int slength = (int)s.length();
	len = WideCharToMultiByte(CP_ACP, 0, s.c_str(), slength, 0, 0, 0, 0);
	std::string r(len, '\0');
	WideCharToMultiByte(CP_ACP, 0, s.c_str(), slength, &r[0], len, 0, 0);
	return r;
}